import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { ProfileView } from './components/ProfileView';
import './custom.css'
import { CreateProfile } from './components/CreateProfile';
import { Exercises } from './components/Exercises';
import { ExerciseView } from './components/ExerciseView';
import { CreateExercise } from './components/CreateExercise';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
            <Route exact path='/' component={Home} />
            <Route exact path='/profile/:id' component={ProfileView} />
            <Route exact path='/newprofile' component={CreateProfile} />
            <Route exact path='/exercise' component={Exercises} />
            <Route exact path='/exercise/:id' component={ExerciseView} />
            <Route exact path='/newexercise' component={CreateExercise} />
      </Layout>
    );
  }
}
