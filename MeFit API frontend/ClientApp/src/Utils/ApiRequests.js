﻿import * as axios from 'axios';

export async function getProfiles() {
    try {
        const result = await axios.get('api/profile')
        return result.data;
    } catch(e) {
        console.error(e);
    }
}

export async function getProfile(id) {
    try {
        const result = await axios.get(`api/profile/${id}`)
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function postProfile(profile) {
    try {
        const result = await axios.post(`api/profile`, profile);
        console.log(result);
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function putProfile(profile) {
    try {
        const result = await axios.put(`api/profile/${profile.id}`, profile);
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function deleteProfile(id) {
    try {
        await axios.delete(`api/profile/${id}`)
    } catch (e) {
        console.error(e);
    }
}

export async function getUsers(onlyAvailable = false) {
    try {
        const result = onlyAvailable ? await axios.get('api/user?onlyAvailable=true') : await axios.get(`api/user`)
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function getAddresses() {
    try {
        const result = await axios.get('api/address')
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function getExercises() {
    try {
        const result = await axios.get('api/exercise');
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function getExercise(id) {
    try {
        const result = await axios.get(`api/exercise/${id}`);
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function postExercise(exercise) {
    try {
        const result = await axios.post('api/exercise', exercise);
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function putExercise(exercise) {
    try {
        const result = await axios.put(`api/exercise/${exercise.id}`, exercise);
        return result.data;
    } catch (e) {
        console.error(e);
    }
}

export async function deleteExercise(id) {
    try {
        await axios.delete(`api/exercise/${id}`);
    } catch (e) {
        console.error(e);
    }
}