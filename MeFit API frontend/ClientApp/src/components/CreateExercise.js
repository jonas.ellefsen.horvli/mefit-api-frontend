﻿import React, { Component } from 'react';
import { history } from '../index';
import { postExercise } from '../Utils/ApiRequests';
import { Exercise } from './Exercise';

export class CreateExercise extends Component {

    handleSubmit = exercise => {
        this.postExercise(exercise);
    }

    async postExercise(profile) {
        const newExercise = await postExercise(profile);
        history.push(`/exercise/${newExercise.id}`);
    }



    render() {
        return (
            <Exercise onSubmit={this.handleSubmit} />
        );
    }
}
