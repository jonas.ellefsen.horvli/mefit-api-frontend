﻿import React, { Component } from 'react';
import { postProfile } from '../Utils/ApiRequests';
import { Profile } from './Profile';
import { history } from '../index';

export class CreateProfile extends Component {

    handleSubmit = profile => {
        this.postProfile(profile);
    }

    async postProfile(profile) {
        const newProfile = await postProfile(profile);
        debugger;
        history.push(`/profile/${newProfile.id}`);
    }
    


    render() {
        return (
            <Profile onSubmit={this.handleSubmit} />
        );
    }
}
