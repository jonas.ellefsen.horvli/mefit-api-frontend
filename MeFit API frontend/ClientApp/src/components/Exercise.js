﻿import React, { Component } from 'react';
import * as renderIf from 'render-if';
import { getExercise } from '../Utils/ApiRequests';

export class Exercise extends Component {
    constructor(props) {
        super(props);
        this.state = { exercise: null, loading: true };
    }

    async componentDidMount() {
        const exerciseId = this.props.exerciseId;
        const exercise = exerciseId ? await getExercise(exerciseId) : {};
        this.setState({ exercise, loading: false });
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        const newExercise = { ...this.state.exercise, [name]: value }
        this.setState({ exercise: newExercise });
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.onSubmit(this.state.exercise);
    }


    render() {
        const { exercise } = this.state;
        return (
            <div>
                {renderIf(this.state.exercise)(() => (
                    <div>
                        <form className="form-group" onSubmit={this.handleSubmit}>
                            <label>Name:</label>
                            <input className="form-control" type="text" name="name" placeholder="Name..." value={exercise.name || ''} onChange={e => this.handleInputChange(e)} required/>
                            <label>Description:</label>
                            <textarea className="form-control" name="description" placeholder="Description..." value={exercise.description} onChange={e => this.handleInputChange(e)} />
                            <label>Target Muscle Group:</label>
                            <input className="form-control" type="text" name="targetMuscleGroup" placeholder="" value={exercise.targetMuscleGroup} onChange={e => this.handleInputChange(e)} />
                            <button className="btnCreate btn btn-primary">{this.props.exerciseId ? 'Save changes' : 'Create Exercise'}</button>
                        </form>
                    </div>
                ))}
            </div>
        );
    }
}
