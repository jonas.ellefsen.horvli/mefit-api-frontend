﻿import React, { Component } from 'react';
import { putExercise } from '../Utils/ApiRequests';
import { Exercise } from './Exercise';

export class ExerciseView extends Component {

    updateExercise = exercise => {
        putExercise(exercise);
        alert('Exercise updated');
    }


    render() {
        return (
            <Exercise exerciseId={this.props.match.params.id} onSubmit={this.updateExercise} />
        );
    }
}
