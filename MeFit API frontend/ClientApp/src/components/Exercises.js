﻿import React, { Component } from 'react';
import './Profiles.styles.scss';
import { Link } from 'react-router-dom';
import { getExercises, deleteExercise } from '../Utils/ApiRequests';

export class Exercises extends Component {
    constructor(props) {
        super(props);
        this.state = { exercises: [], loading: true };
    }

    async componentDidMount() {
        const exercises = await getExercises();
        this.setState({ exercises, loading: false });
    }

    async deleteExercise(id) {
        await deleteExercise(id);

        const exercises = this.state.exercises.filter(exercise => exercise.id !== id);
        this.setState({ exercises });
    }

    deleteHandleClick(exercise) {
        window.confirm(`Are you sure you want to delete exercise ${exercise.name}?`) && this.deleteExercise(exercise.id);
    }

    render() {
        return (
            <table className="profilesTable">
                <thead>
                    <tr>
                        <td className="profileId">ID</td>
                        <td className="profileFirstName">Name</td>
                        <td className="profileLastName">Description</td>
                        <td className="profileWeight">Target Muscle Group</td>
                    </tr>
                </thead>
                <tbody>
                    {this.state.exercises.map((exercise, i) => {
                        return (
                            <tr key={i}>
                                <td>{exercise.id}</td>
                                <td>{exercise.name}</td>
                                <td>{exercise.description}</td>
                                <td>{exercise.targetMuscleGroup}</td>
                                <td className="btnGroup">
                                    <Link to={`/exercise/${exercise.id}`} className="btnProfile">View</Link>
                                    <button type="button" className="btnDelete close" aria-label="Close" onClick={() => this.deleteHandleClick(exercise)}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        );
    }
}

