import React, { Component } from 'react';
import { Profiles } from './Profiles';

export class Home extends Component {

  render () {
    return (
        <div>
            <Profiles />
        </div>
    );
  }
}
