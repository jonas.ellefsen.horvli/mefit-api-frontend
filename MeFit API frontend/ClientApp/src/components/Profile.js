﻿import React, { Component } from 'react';
import { getProfile } from '../Utils/ApiRequests';
import * as renderIf from 'render-if';
import { SelectUser } from './SelectUser';
import { SelectAddress } from './SelectAddress';

export class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = { profile: null, loading: true };
    }

    async componentDidMount() {
        const profileId = this.props.profileId;
        const profile = profileId ? await getProfile(profileId) : { user: {}, address: {} };
        this.setState({ profile, loading: false });
    }

    selectUser(user) {
        const updatedProfile = { ...this.state.profile, user }
        updatedProfile.userId = user.id;
        this.setState({ profile: updatedProfile})
    }

    selectAddress(address) {
        const updatedProfile = { ...this.state.profile, address }
        updatedProfile.addressId = address.id;
        this.setState({ profile: updatedProfile })
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        let value;
        switch (name) {
            case 'height':
            case 'weight':
                value = parseFloat(target.value);
                break;
            default:
                value = target.value;
        }
        const newProfile = { ...this.state.profile, [name]: value }
        this.setState({ profile: newProfile });
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.onSubmit(this.state.profile);
    }


    render() {
        const { profile } = this.state;
        return (
            <div>
                {renderIf(this.state.profile)(() => (
                    <div>
                        <form className="form-group" onSubmit={this.handleSubmit}>
                            <SelectUser user={profile.user} selectUser={(user) => this.selectUser(user)} />
                            <SelectAddress address={profile.address} selectAddress={(address) => this.selectAddress(address)} />
                            <label>Height:</label>
                            <input className="form-control" type="number" name="height" placeholder="" value={profile.height || ""} min={0} max={5} step={0.05} onChange={e => this.handleInputChange(e)} />
                            <small className="form-text text-muted">meters</small>
                            <label>Weight:</label>
                            <input className="form-control" type="text" name="weight" placeholder="" value={profile.weight || ""} min={0} max={1000} onChange={e => this.handleInputChange(e)} />
                            <small className="form-text text-muted">kilograms</small>
                            <label>Medical Conditions:</label>
                            <input className="form-control" type="text" name="medicalConditions" placeholder="" value={profile.medicalConditions} onChange={e => this.handleInputChange(e)} />
                            <label>Disabilities:</label>
                            <input className="form-control" type="text" name="disabilities" placeholder="" value={profile.disabilities} onChange={e => this.handleInputChange(e)} />
                            <button className="btnCreate btn btn-primary">{this.props.profileId ? 'Save changes' : 'Create Profile'}</button>
                        </form>
                    </div>
                ))}
            </div>
        );
    }
}
