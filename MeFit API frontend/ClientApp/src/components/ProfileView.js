﻿import React, { Component } from 'react';
import { putProfile } from '../Utils/ApiRequests';
import { Profile } from './Profile';

export class ProfileView extends Component {

    updateProfile = profile => {
        putProfile(profile);
        alert('Profile updated');
    }


    render() {
        return (
            <Profile profileId={this.props.match.params.id} onSubmit={this.updateProfile} />
        );
    }
}
