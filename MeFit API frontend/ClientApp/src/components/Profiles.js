﻿import React, { Component } from 'react';
import './Profiles.styles.scss';
import { getProfiles, deleteProfile } from '../Utils/ApiRequests';
import { Link } from 'react-router-dom';

export class Profiles extends Component {
    constructor(props) {
        super(props);
        this.state = { profiles: [], loading: true };
    }

    async componentDidMount() {
        const profiles = await getProfiles();
        this.setState({ profiles, loading: false });
    }

    async deleteProfile(id) {
        await deleteProfile(id);

        const profiles = this.state.profiles.filter(p => p.id !== id);
        this.setState({ profiles });
    }

    deleteHandleClick(profile) {
        window.confirm(`Are you sure you want to delete profile ${profile.user.firstName} ${profile.user.lastName}?`) && this.deleteProfile(profile.id);
    }

    render() {
        return (
            <table className="profilesTable">
                <thead>
                    <tr>
                        <td className="profileId">ID</td>
                        <td className="profileFirstName">First name</td>
                        <td className="profileLastName">Last name</td>
                        <td className="profileWeight">Weight</td>
                        <td className="profileHeight">Height</td>
                    </tr>
                </thead>
                <tbody>
                    {this.state.profiles.map((p, i) => {
                        return (
                            <tr key={i}>
                                <td>{p.id}</td>
                                <td>{p.user.firstName}</td>
                                <td>{p.user.lastName}</td>
                                <td>{p.weight}</td>
                                <td>{p.height}</td>
                                <td className="btnGroup">
                                    <Link to={`/profile/${p.id}`} className="btnProfile">View</Link>
                                    <button type="button" className="btnDelete close" aria-label="Close" onClick={() => this.deleteHandleClick(p)}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        );
    }
}

// {this.state.profiles.map((p, i) => <div key={i}>{p.user.firstName}</div>)}
