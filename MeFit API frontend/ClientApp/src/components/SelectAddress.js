﻿import React, { Component } from 'react';
import { getAddresses } from '../Utils/ApiRequests';
import * as renderIf from 'render-if';

export class SelectAddress extends Component {
    constructor(props) {
        super(props);
        this.state = { selectedAddress: null, addresses: null, loading: true };
    }

    async componentDidMount() {
        const addresses = await getAddresses();
        this.setState({ addresses, loading: false });
    }

    selectAddress(event) {
        let selectedAddress = this.state.addresses.find(address => address.id === parseInt(event.target.value));
        if (!selectedAddress) {
            selectedAddress = { addressLine1: "", addressLine2: "", addressLine3: "", postalCode: null, city: "", country: "" }
        }
        this.props.selectAddress(selectedAddress);
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        const newAddress = { ...this.props.address, [name]: value }
        this.props.selectAddress(newAddress);
    }

    render() {
        return (
            <div>
                {renderIf(!this.state.loading)(() => (
                    <div>
                        <label>Select Address:</label>
                        <select className="form-control" defaultValue={this.props.address.id} onChange={e => this.selectAddress(e)}>
                            <option value="">New address</option>
                            {this.state.addresses.map(address => <option key={address.id} value={address.id}>{address.id}</option>)}
                        </select>
                        <label>Address:</label>
                        <input className="form-control" type="text" name="addressLine1" placeholder="" readOnly={this.props.address.id} value={this.props.address.addressLine1 || ""} onChange={e => this.handleInputChange(e)} required />
                        <input className="form-control" type="text" name="addressLine2" placeholder="" readOnly={this.props.address.id} value={this.props.address.addressLine2 || ""} onChange={e => this.handleInputChange(e)} />
                        <input className="form-control" type="text" name="addressLine3" placeholder="" readOnly={this.props.address.id} value={this.props.address.addressLine3 || ""} onChange={e => this.handleInputChange(e)} />
                        <label>Postal Code:</label>
                        <input className="form-control" type="text" name="postalCode" placeholder="postal code.." readOnly={this.props.address.id} value={this.props.address.postalCode || ""} onChange={e => this.handleInputChange(e)} required />
                        <label>Country:</label>
                        <input className="form-control" type="text" name="country" placeholder="country.." readOnly={this.props.address.id} value={this.props.address.country} onChange={e => this.handleInputChange(e)} required />
                    </div>
                ))}
            </div>
        );
    }
}
