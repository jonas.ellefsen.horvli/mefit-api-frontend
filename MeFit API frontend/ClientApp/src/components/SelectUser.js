﻿import React, { Component } from 'react';
import { getUsers } from '../Utils/ApiRequests';
import * as renderIf from 'render-if';
import * as _ from 'lodash';

export class SelectUser extends Component {
    constructor(props) {
        super(props);
        this.state = { selectedUser: null, users: null, loading: true };
    }

    async componentDidMount() {
        let users = await getUsers(true);
        if (this.props.user) {
            users.push(this.props.user);
            users = _.sortBy(users, user => user.id);
        }
        this.setState({ users, loading: false });
    }

    selectUser(event) {
        let selectedUser = this.state.users.find(user => user.id === parseInt(event.target.value));
        if (!selectedUser) {
            selectedUser = { firstName: "", lastName: "", password: "" }
        }
        this.props.selectUser(selectedUser);
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        let value;

        switch (name) {
            case 'isContributor':
            case 'isAdmin':
                value = target.checked;
                break;
            default:
                value = target.value;
        }

        const newUser = { ...this.props.user, [name]: value }
        this.props.selectUser(newUser);
    }

    render() {
        const { user } = this.props;
        return (
            <div>
                {renderIf(!this.state.loading)(() => (
                    <div>
                        <label>Select User:</label>
                        <select className="form-control" id="sel1" defaultValue={user ? user.id : ''} onChange={e => this.selectUser(e)}>
                            <option value="">New user</option>
                            {this.state.users.map(user => <option key={user.id} value={user.id}>{`${user.id}: ${user.firstName} ${user.lastName}`}</option>)}
                        </select>
                        <label>First name:</label>
                        <input className="form-control" type="text" name="firstName" placeholder="First name" readOnly={this.props.user.id} value={this.props.user.firstName || ''} onChange={e => this.handleInputChange(e)} required/>
                        <label>Last name:</label>
                        <input className="form-control" type="text" name="lastName" placeholder="Last name" readOnly={this.props.user.id} value={this.props.user.lastName || ''} onChange={e => this.handleInputChange(e)} required/>
                        <label>Password:</label>
                        <input className="form-control" type="password" name="password" placeholder="" readOnly={this.props.user.id} value={this.props.user.password || ''} onChange={e => this.handleInputChange(e)} />
                        <div className="checkbox">
                            <input className="checkbox-input" type="checkbox" name="isContributor" disabled={this.props.user.id} value={this.props.user.isContributor} onChange={e => this.handleInputChange(e)} />
                            <label className="checkbox-label">Contributor</label>
                        </div>
                        <div className="checkbox">
                            <input className="checkbox-input" type="checkbox" name="isAdmin" disabled={this.props.user.id} value={this.props.user.isAdmin} onChange={e => this.handleInputChange(e)} />
                            <label className="checkbox-label">Admin</label>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}
