﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MeFitAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GoalController : ControllerBase
    {
        private readonly MeFitContext _context;

        public GoalController(MeFitContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Goal>> GetGoal()
        {
            return _context.Goals;
        }

        [HttpGet("{id}")]
        public ActionResult<Goal> GetGoalById(int id)
        {
            return _context.Goals.Include(goal => goal.GoalWorkouts).Where(goal => goal.Id == id).FirstOrDefault();
        }

        [HttpPost]
        public ActionResult<Goal> PostGoal(Goal goal)
        {
            _context.Goals.Add(goal);
            _context.SaveChanges();

            return CreatedAtAction("GetGoalById", new Goal { Id = goal.Id }, goal);
        }

        [HttpPut("{id}")]
        public ActionResult<Goal> PutGoal(int id, Goal goal)
        {
            if (id != goal.Id)
            {
                return BadRequest();
            }
            _context.Entry(goal).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<Goal> DeleteGoal(int id)
        {
            Goal goal = _context.Goals.Find(id);

            if (goal == null)
            {
                return NotFound();
            }

            _context.Goals.Remove(goal);
            _context.SaveChanges();

            return goal;
        }
    }
}